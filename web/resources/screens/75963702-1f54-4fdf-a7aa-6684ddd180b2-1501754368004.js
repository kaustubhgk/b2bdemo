jQuery("#simulation")
  .on("click", ".s-75963702-1f54-4fdf-a7aa-6684ddd180b2 .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Hotspot_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/4862ccc8-3824-4cbf-9fbb-89a3a496b0df"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Hotspot_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/4862ccc8-3824-4cbf-9fbb-89a3a496b0df"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("mouseenter dragenter", ".s-75963702-1f54-4fdf-a7aa-6684ddd180b2 .mouseenter", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Label_6") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_6": {
                      "attributes": {
                        "width": "jimEvent.fn.getCurrentStyle('width', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_6') + jimEvent.fn.getCurrentStyle('padding-left', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_6') + jimEvent.fn.getCurrentStyle('padding-right', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_6') - 14 - 0",
                        "height": "jimEvent.fn.getCurrentStyle('height', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_6') + jimEvent.fn.getCurrentStyle('padding-top', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_6') + jimEvent.fn.getCurrentStyle('padding-bottom', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_6') - 0 - 0",
                        "padding-top": "2px",
                        "padding-right": "2px",
                        "padding-bottom": "2px",
                        "padding-left": "16px",
                        "background-color": "#EEEEEE",
                        "background-image": "none",
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#EEEEEE",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#EEEEEE",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#EEEEEE",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#EEEEEE",
                        "border-radius": "3px 3px 3px 3px"
                      },
                      "expressions": {
                        "width": "Math.max(130 - 1 - 1 - 16 - 2, 0) + 'px'",
                        "height": "Math.max(28 - 1 - 1 - 2 - 2, 0) + 'px'"
                      }
                    }
                  },{
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_6": {
                      "attributes-ie": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#EEEEEE",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#EEEEEE",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#EEEEEE",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#EEEEEE",
                        "border-radius": "3px 3px 3px 3px",
                        "padding-top": "2px",
                        "padding-right": "2px",
                        "padding-bottom": "2px",
                        "padding-left": "16px",
                        "-pie-background": "#EEEEEE",
                        "-pie-poll": "false"
                      },
                      "expressions-ie": {
                        "width": "Math.max(130 - 1 - 1 - 16 - 2, 0) + 'px'",
                        "height": "Math.max(28 - 1 - 1 - 2 - 2, 0) + 'px'"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Label_7") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_7": {
                      "attributes": {
                        "width": "jimEvent.fn.getCurrentStyle('width', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_7') + jimEvent.fn.getCurrentStyle('padding-left', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_7') + jimEvent.fn.getCurrentStyle('padding-right', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_7') - 14 - 0",
                        "height": "jimEvent.fn.getCurrentStyle('height', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_7') + jimEvent.fn.getCurrentStyle('padding-top', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_7') + jimEvent.fn.getCurrentStyle('padding-bottom', '#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_7') - 0 - 0",
                        "padding-top": "2px",
                        "padding-right": "2px",
                        "padding-bottom": "2px",
                        "padding-left": "16px",
                        "background-color": "#EEEEEE",
                        "background-image": "none",
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#EEEEEE",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#EEEEEE",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#EEEEEE",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#EEEEEE",
                        "border-radius": "3px 3px 3px 3px"
                      },
                      "expressions": {
                        "width": "Math.max(130 - 1 - 1 - 16 - 2, 0) + 'px'",
                        "height": "Math.max(28 - 1 - 1 - 2 - 2, 0) + 'px'"
                      }
                    }
                  },{
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_7": {
                      "attributes-ie": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#EEEEEE",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#EEEEEE",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#EEEEEE",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#EEEEEE",
                        "border-radius": "3px 3px 3px 3px",
                        "padding-top": "2px",
                        "padding-right": "2px",
                        "padding-bottom": "2px",
                        "padding-left": "16px",
                        "-pie-background": "#EEEEEE",
                        "-pie-poll": "false"
                      },
                      "expressions-ie": {
                        "width": "Math.max(130 - 1 - 1 - 16 - 2, 0) + 'px'",
                        "height": "Math.max(28 - 1 - 1 - 2 - 2, 0) + 'px'"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Label_40") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_40": {
                      "attributes": {
                        "background-color": "#F5F5F5",
                        "background-image": "none",
                        "font-size": "11.0pt",
                        "font-family": "'Arial',Arial"
                      }
                    }
                  },{
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_40 .valign": {
                      "attributes": {
                        "vertical-align": "middle",
                        "text-align": "center"
                      }
                    }
                  },{
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_40 span": {
                      "attributes": {
                        "color": "#005580",
                        "text-align": "center",
                        "text-decoration": "none",
                        "font-family": "'Arial',Arial",
                        "font-size": "11.0pt",
                        "font-style": "normal",
                        "font-weight": "400"
                      }
                    }
                  },{
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_40": {
                      "attributes-ie": {
                        "-pie-background": "#F5F5F5",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Label_41") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_41": {
                      "attributes": {
                        "background-color": "#F5F5F5",
                        "background-image": "none",
                        "font-size": "11.0pt",
                        "font-family": "'Arial',Arial"
                      }
                    }
                  },{
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_41 .valign": {
                      "attributes": {
                        "vertical-align": "middle",
                        "text-align": "center"
                      }
                    }
                  },{
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_41 span": {
                      "attributes": {
                        "color": "#005580",
                        "text-align": "center",
                        "text-decoration": "none",
                        "font-family": "'Arial',Arial",
                        "font-size": "11.0pt",
                        "font-style": "normal",
                        "font-weight": "400"
                      }
                    }
                  },{
                    "#s-75963702-1f54-4fdf-a7aa-6684ddd180b2 #s-Label_41": {
                      "attributes-ie": {
                        "-pie-background": "#F5F5F5",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    }
  })
  .on("mouseleave dragleave", ".s-75963702-1f54-4fdf-a7aa-6684ddd180b2 .mouseleave", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Label_6")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Label_7")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Label_40")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Label_41")) {
      jEvent.undoCases(jFirer);
    }
  });