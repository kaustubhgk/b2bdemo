jQuery("#simulation")
  .on("click", ".s-8d6ab638-4bce-482f-b180-70d82e7df213 .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Label_29")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/caa0fc67-2b43-42a7-88cc-bf2ca6f7fdc8"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Label_30")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/9e0e72f4-eb07-47fc-8f92-56689676df9e"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Hotspot_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/1098cc94-5ef2-40f8-ae7b-a737dd890642"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("mouseenter dragenter", ".s-8d6ab638-4bce-482f-b180-70d82e7df213 .mouseenter", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Label_29") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_29": {
                      "attributes": {
                        "width": "jimEvent.fn.getCurrentStyle('width', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_29') + jimEvent.fn.getCurrentStyle('padding-left', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_29') + jimEvent.fn.getCurrentStyle('padding-right', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_29') - 5 - 5",
                        "height": "jimEvent.fn.getCurrentStyle('height', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_29') + jimEvent.fn.getCurrentStyle('padding-top', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_29') + jimEvent.fn.getCurrentStyle('padding-bottom', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_29') - 5 - 5",
                        "padding-top": "7px",
                        "padding-right": "7px",
                        "padding-bottom": "7px",
                        "padding-left": "7px",
                        "background-color": "#FFFFFF",
                        "background-image": "none",
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#D4D4D4",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#D4D4D4",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#D4D4D4",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#D4D4D4",
                        "border-radius": "4px 4px 4px 4px",
                        "font-size": "11.0pt",
                        "font-family": "'Arial',Arial"
                      },
                      "expressions": {
                        "width": "Math.max(70 - 1 - 1 - 7 - 7, 0) + 'px'",
                        "height": "Math.max(42 - 1 - 1 - 7 - 7, 0) + 'px'"
                      }
                    }
                  },{
                    "#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_29 .valign": {
                      "attributes": {
                        "vertical-align": "middle",
                        "text-align": "center"
                      }
                    }
                  },{
                    "#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_29 span": {
                      "attributes": {
                        "color": "#000000",
                        "text-align": "center",
                        "text-decoration": "none",
                        "font-family": "'Arial',Arial",
                        "font-size": "11.0pt",
                        "font-style": "normal",
                        "font-weight": "400"
                      }
                    }
                  },{
                    "#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_29": {
                      "attributes-ie": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#D4D4D4",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#D4D4D4",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#D4D4D4",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#D4D4D4",
                        "border-radius": "4px 4px 4px 4px",
                        "padding-top": "7px",
                        "padding-right": "7px",
                        "padding-bottom": "7px",
                        "padding-left": "7px",
                        "-pie-background": "#FFFFFF",
                        "-pie-poll": "false"
                      },
                      "expressions-ie": {
                        "width": "Math.max(70 - 1 - 1 - 7 - 7, 0) + 'px'",
                        "height": "Math.max(42 - 1 - 1 - 7 - 7, 0) + 'px'"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Label_30") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_30": {
                      "attributes": {
                        "width": "jimEvent.fn.getCurrentStyle('width', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_30') + jimEvent.fn.getCurrentStyle('padding-left', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_30') + jimEvent.fn.getCurrentStyle('padding-right', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_30') - 5 - 5",
                        "height": "jimEvent.fn.getCurrentStyle('height', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_30') + jimEvent.fn.getCurrentStyle('padding-top', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_30') + jimEvent.fn.getCurrentStyle('padding-bottom', '#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_30') - 5 - 5",
                        "padding-top": "7px",
                        "padding-right": "7px",
                        "padding-bottom": "7px",
                        "padding-left": "7px",
                        "background-color": "#FFFFFF",
                        "background-image": "none",
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#FFFFFF",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#FFFFFF",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#FFFFFF",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#FFFFFF",
                        "border-radius": "4px 4px 4px 4px",
                        "font-size": "11.0pt",
                        "font-family": "'Arial',Arial"
                      },
                      "expressions": {
                        "width": "Math.max(84 - 1 - 1 - 7 - 7, 0) + 'px'",
                        "height": "Math.max(42 - 1 - 1 - 7 - 7, 0) + 'px'"
                      }
                    }
                  },{
                    "#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_30 .valign": {
                      "attributes": {
                        "vertical-align": "middle",
                        "text-align": "center"
                      }
                    }
                  },{
                    "#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_30 span": {
                      "attributes": {
                        "color": "#000000",
                        "text-align": "center",
                        "text-decoration": "none",
                        "font-family": "'Arial',Arial",
                        "font-size": "11.0pt",
                        "font-style": "normal",
                        "font-weight": "400"
                      }
                    }
                  },{
                    "#s-8d6ab638-4bce-482f-b180-70d82e7df213 #s-Label_30": {
                      "attributes-ie": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#FFFFFF",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#FFFFFF",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#FFFFFF",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#FFFFFF",
                        "border-radius": "4px 4px 4px 4px",
                        "padding-top": "7px",
                        "padding-right": "7px",
                        "padding-bottom": "7px",
                        "padding-left": "7px",
                        "-pie-background": "#FFFFFF",
                        "-pie-poll": "false"
                      },
                      "expressions-ie": {
                        "width": "Math.max(84 - 1 - 1 - 7 - 7, 0) + 'px'",
                        "height": "Math.max(42 - 1 - 1 - 7 - 7, 0) + 'px'"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    }
  })
  .on("mouseleave dragleave", ".s-8d6ab638-4bce-482f-b180-70d82e7df213 .mouseleave", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Label_29")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Label_30")) {
      jEvent.undoCases(jFirer);
    }
  });