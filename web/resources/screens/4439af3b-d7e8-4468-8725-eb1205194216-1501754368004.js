jQuery("#simulation")
  .on("click", ".s-4439af3b-d7e8-4468-8725-eb1205194216 .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Label_29")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/caa0fc67-2b43-42a7-88cc-bf2ca6f7fdc8"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Hotspot_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/1098cc94-5ef2-40f8-ae7b-a737dd890642"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("mouseenter dragenter", ".s-4439af3b-d7e8-4468-8725-eb1205194216 .mouseenter", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Label_29") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-4439af3b-d7e8-4468-8725-eb1205194216 #s-Label_29": {
                      "attributes": {
                        "width": "jimEvent.fn.getCurrentStyle('width', '#s-4439af3b-d7e8-4468-8725-eb1205194216 #s-Label_29') + jimEvent.fn.getCurrentStyle('padding-left', '#s-4439af3b-d7e8-4468-8725-eb1205194216 #s-Label_29') + jimEvent.fn.getCurrentStyle('padding-right', '#s-4439af3b-d7e8-4468-8725-eb1205194216 #s-Label_29') - 5 - 5",
                        "height": "jimEvent.fn.getCurrentStyle('height', '#s-4439af3b-d7e8-4468-8725-eb1205194216 #s-Label_29') + jimEvent.fn.getCurrentStyle('padding-top', '#s-4439af3b-d7e8-4468-8725-eb1205194216 #s-Label_29') + jimEvent.fn.getCurrentStyle('padding-bottom', '#s-4439af3b-d7e8-4468-8725-eb1205194216 #s-Label_29') - 5 - 5",
                        "padding-top": "7px",
                        "padding-right": "7px",
                        "padding-bottom": "7px",
                        "padding-left": "7px",
                        "background-color": "#FFFFFF",
                        "background-image": "none",
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#D4D4D4",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#D4D4D4",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#D4D4D4",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#D4D4D4",
                        "border-radius": "4px 4px 4px 4px",
                        "font-size": "11.0pt",
                        "font-family": "'Arial',Arial"
                      },
                      "expressions": {
                        "width": "Math.max(70 - 1 - 1 - 7 - 7, 0) + 'px'",
                        "height": "Math.max(42 - 1 - 1 - 7 - 7, 0) + 'px'"
                      }
                    }
                  },{
                    "#s-4439af3b-d7e8-4468-8725-eb1205194216 #s-Label_29 .valign": {
                      "attributes": {
                        "vertical-align": "middle",
                        "text-align": "center"
                      }
                    }
                  },{
                    "#s-4439af3b-d7e8-4468-8725-eb1205194216 #s-Label_29 span": {
                      "attributes": {
                        "color": "#000000",
                        "text-align": "center",
                        "text-decoration": "none",
                        "font-family": "'Arial',Arial",
                        "font-size": "11.0pt",
                        "font-style": "normal",
                        "font-weight": "400"
                      }
                    }
                  },{
                    "#s-4439af3b-d7e8-4468-8725-eb1205194216 #s-Label_29": {
                      "attributes-ie": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#D4D4D4",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#D4D4D4",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#D4D4D4",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#D4D4D4",
                        "border-radius": "4px 4px 4px 4px",
                        "padding-top": "7px",
                        "padding-right": "7px",
                        "padding-bottom": "7px",
                        "padding-left": "7px",
                        "-pie-background": "#FFFFFF",
                        "-pie-poll": "false"
                      },
                      "expressions-ie": {
                        "width": "Math.max(70 - 1 - 1 - 7 - 7, 0) + 'px'",
                        "height": "Math.max(42 - 1 - 1 - 7 - 7, 0) + 'px'"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    }
  })
  .on("mouseleave dragleave", ".s-4439af3b-d7e8-4468-8725-eb1205194216 .mouseleave", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Label_29")) {
      jEvent.undoCases(jFirer);
    }
  });