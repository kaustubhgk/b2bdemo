(function(window, undefined) {

  var jimLinks = {
    "3e3fd30c-21e7-49d8-9334-f6f956edb8ee" : {
      "Text_cell_2" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Text_cell_3" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Text_cell_16" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Text_cell_21" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Text_cell_26" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Text_cell_31" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Text_cell_36" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Button_1" : [
        "ac613dff-baf2-4c47-9c4e-97fbb5900a61"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ],
      "Label_2" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_5" : [
        "b434b8ed-70b1-4325-b4dd-447c6acc36b8"
      ],
      "Label_7" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ],
      "Image_19" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Image_23" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Image_25" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Image_27" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Image_29" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Image_31" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ],
      "Image_33" : [
        "15eedc32-ba1d-4908-838d-4c2fa60e9eaa"
      ]
    },
    "7c82acbb-8012-48b0-90f4-b02ace645e77" : {
      "Hotspot_11" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_1" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_2" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_3" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_4" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_6" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_7" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_8" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_9" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_10" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_12" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_13" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_14" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_15" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_16" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_17" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_18" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_19" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_20" : [
        "865dc106-9535-4423-b66e-cc9c3570b79e"
      ],
      "Hotspot_5" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "842e7a3c-e3da-49b2-bd59-f30ea0b1d9cd" : {
      "Button_2" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ],
      "Button_3" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ]
    },
    "6fc14fd2-60fa-45fc-8587-d77ccd98071d" : {
      "Hotspot_5" : [
        "7bec2ebd-9342-4191-881f-cf4ea538570c"
      ],
      "Hotspot_13" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ],
      "Hotspot_1" : [
        "7bec2ebd-9342-4191-881f-cf4ea538570c"
      ],
      "Hotspot_2" : [
        "7bec2ebd-9342-4191-881f-cf4ea538570c"
      ],
      "Hotspot_3" : [
        "7bec2ebd-9342-4191-881f-cf4ea538570c"
      ],
      "Hotspot_4" : [
        "7bec2ebd-9342-4191-881f-cf4ea538570c"
      ],
      "Hotspot_6" : [
        "7bec2ebd-9342-4191-881f-cf4ea538570c"
      ],
      "Hotspot_7" : [
        "7bec2ebd-9342-4191-881f-cf4ea538570c"
      ],
      "Hotspot_8" : [
        "7bec2ebd-9342-4191-881f-cf4ea538570c"
      ],
      "Hotspot_9" : [
        "7bec2ebd-9342-4191-881f-cf4ea538570c"
      ]
    },
    "5f412310-0af9-462b-a8f5-f46cb1f31e7c" : {
      "Input_11" : [
        "5f412310-0af9-462b-a8f5-f46cb1f31e7c"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ],
      "Rectangle_12" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ]
    },
    "816d8f26-8520-4749-867f-c48008e0bbca" : {
      "Button_3" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Button_4" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ],
      "Label_2" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_7" : [
        "b434b8ed-70b1-4325-b4dd-447c6acc36b8"
      ],
      "Label_5" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ]
    },
    "2fd50b8b-8591-4968-a722-5f7a2a79dfb4" : {
      "Button_1" : [
        "46eb1013-e2d1-477b-a6e6-c797d3c4a4bb"
      ],
      "Hotspot_2" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_3" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_4" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_5" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_6" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_7" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_8" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_9" : [
        "62748e92-3f1b-4bca-9e5c-4d72c7c90c70"
      ],
      "Hotspot_10" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_11" : [
        "62748e92-3f1b-4bca-9e5c-4d72c7c90c70"
      ],
      "Hotspot_12" : [
        "62748e92-3f1b-4bca-9e5c-4d72c7c90c70"
      ],
      "Hotspot_13" : [
        "62748e92-3f1b-4bca-9e5c-4d72c7c90c70"
      ],
      "Hotspot_14" : [
        "62748e92-3f1b-4bca-9e5c-4d72c7c90c70"
      ],
      "Hotspot_15" : [
        "62748e92-3f1b-4bca-9e5c-4d72c7c90c70"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "63fdf96d-7f58-42cd-997e-788220726745" : {
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ],
      "Label_2" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_5" : [
        "b434b8ed-70b1-4325-b4dd-447c6acc36b8"
      ],
      "Label_7" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ]
    },
    "be5e8282-09ce-44d0-90c1-3492763211b3" : {
      "Label_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ]
    },
    "152edac9-417e-44ad-a719-06621542766b" : {
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "06a617d2-0b0e-4bd5-b95c-31a1cf1badc2" : {
      "Label_7" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ],
      "Label_2" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_1" : [
        "b434b8ed-70b1-4325-b4dd-447c6acc36b8"
      ],
      "Label_5" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Text_45" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ]
    },
    "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8" : {
      "Hotspot_1" : [
        "dcc5a46f-47b9-46c6-8eb0-b772ddb1347c"
      ],
      "Hotspot_2" : [
        "dcc5a46f-47b9-46c6-8eb0-b772ddb1347c"
      ]
    },
    "89e9fcfa-89b2-475b-b837-d1ac90b3b22d" : {
      "Hotspot_11" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_1" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_2" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_3" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_4" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_5" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_6" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_7" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_8" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_9" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_10" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_12" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "e6c7eed4-c07c-4af4-a794-a03fe16757a2" : {
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_8" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_9" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ],
      "Rectangle_1" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Image_19" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Text_23" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Image_20" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Image_21" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Button_1" : [
        "47f199bb-ed47-48c3-9b3c-91d46b2f5f55"
      ]
    },
    "caa0fc67-2b43-42a7-88cc-bf2ca6f7fdc8" : {
      "Image_2" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_3" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_4" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_5" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_6" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_7" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_8" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_9" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Label_30" : [
        "9e0e72f4-eb07-47fc-8f92-56689676df9e"
      ],
      "Label_31" : [
        "8d6ab638-4bce-482f-b180-70d82e7df213"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ]
    },
    "ce5b346a-2904-4724-a2b1-397bb374e184" : {
      "Button_3" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Button_4" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "8d6ab638-4bce-482f-b180-70d82e7df213" : {
      "Label_29" : [
        "caa0fc67-2b43-42a7-88cc-bf2ca6f7fdc8"
      ],
      "Label_30" : [
        "9e0e72f4-eb07-47fc-8f92-56689676df9e"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ]
    },
    "ac613dff-baf2-4c47-9c4e-97fbb5900a61" : {
      "Button_3" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Button_4" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ],
      "Label_2" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_5" : [
        "b434b8ed-70b1-4325-b4dd-447c6acc36b8"
      ],
      "Label_7" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ]
    },
    "15eedc32-ba1d-4908-838d-4c2fa60e9eaa" : {
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ],
      "Label_2" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_5" : [
        "b434b8ed-70b1-4325-b4dd-447c6acc36b8"
      ],
      "Label_7" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ]
    },
    "98f246bc-68da-4efe-95cc-f69e6b0545b9" : {
      "Button_1" : [
        "b5d3ee73-97b8-42cc-8648-46a63c7ef8b1"
      ],
      "Hotspot_2" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_3" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_4" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_5" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_6" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_7" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_8" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_9" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_10" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_11" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_12" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_13" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_14" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_15" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "55302c98-dacd-409b-aca9-b66bde66bb22" : {
      "Button_3" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Button_4" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "3208cb01-2887-4b59-98d3-6b631eba0a60" : {
      "Button_3" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "62052b62-0e08-4385-b811-459d6e3bd458" : {
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "0bc83ef7-b6c9-4707-842b-ec178414f6d4" : {
      "Button_3" : [
        "05556a4b-b7db-407b-92a0-3a5db0eea992"
      ],
      "Button_4" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_37" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_8" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_7" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ]
    },
    "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac" : {
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "05556a4b-b7db-407b-92a0-3a5db0eea992" : {
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_8" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_7" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ],
      "Button_7" : [
        "816d8f26-8520-4749-867f-c48008e0bbca"
      ]
    },
    "76414bff-23a4-49bf-86d8-eee2b50fc955" : {
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ],
      "Text_49" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ]
    },
    "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96" : {
      "Text_cell_30" : [
        "e4f289e1-cd8d-44f4-b911-42fce351a3cf"
      ],
      "Text_cell_46" : [
        "e2befb41-ce34-467e-b1f0-76742ef625c5"
      ],
      "Hotspot_2" : [
        "f0681338-9481-448b-889a-96e76a81eab8"
      ],
      "Hotspot_3" : [
        "246d0410-edea-4da3-9af7-c856145710e2"
      ],
      "Hotspot_4" : [
        "ef016c51-f4bb-4ea9-b0c9-81f8e2a0c574"
      ],
      "Hotspot_5" : [
        "4862ccc8-3824-4cbf-9fbb-89a3a496b0df"
      ],
      "Hotspot_6" : [
        "7c82acbb-8012-48b0-90f4-b02ace645e77"
      ],
      "Hotspot_7" : [
        "dcc5a46f-47b9-46c6-8eb0-b772ddb1347c"
      ],
      "Hotspot_9" : [
        "f61420f4-0edf-48af-8504-e3daa782c825"
      ],
      "Hotspot_10" : [
        "7bec2ebd-9342-4191-881f-cf4ea538570c"
      ],
      "Hotspot_11" : [
        "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Image_22" : [
        "e2befb41-ce34-467e-b1f0-76742ef625c5"
      ],
      "Label_8" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_9" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_12" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "62748e92-3f1b-4bca-9e5c-4d72c7c90c70" : {
      "Button_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "b7660e1d-2981-480e-a434-643a6005623d" : {
      "Text_cell_2" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Text_cell_3" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Text_cell_16" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Text_cell_21" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Text_cell_26" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Text_cell_31" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Text_cell_36" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Image_2" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Image_4" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Image_6" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Image_8" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Image_10" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Image_12" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Image_14" : [
        "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0"
      ],
      "Button_1" : [
        "842e7a3c-e3da-49b2-bd59-f30ea0b1d9cd"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ]
    },
    "7bec2ebd-9342-4191-881f-cf4ea538570c" : {
      "Hotspot_1" : [
        "6fc14fd2-60fa-45fc-8587-d77ccd98071d"
      ],
      "Hotspot_2" : [
        "6fc14fd2-60fa-45fc-8587-d77ccd98071d"
      ]
    },
    "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d" : {
      "Button_3" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "c0aac25b-c71e-42ef-9cdd-d204b7efc452" : {
      "Button_3" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Button_4" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "8a9c867b-1bad-4d75-9d7d-ab845d5dd386" : {
      "Text_cell_2" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Text_cell_3" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Text_cell_16" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Text_cell_21" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Text_cell_26" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Text_cell_31" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Text_cell_36" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Image_2" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Image_4" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Image_6" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Image_8" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Image_10" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Image_12" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Image_14" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Button_1" : [
        "c0aac25b-c71e-42ef-9cdd-d204b7efc452"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "d8b4ac47-3750-4dd2-9882-69163287b8ff" : {
      "Label_9" : [
        "0443c108-a6d5-4f3e-bfbf-a1d249d14d09"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "0443c108-a6d5-4f3e-bfbf-a1d249d14d09"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "bc9194cf-4d3e-49e0-829b-adaa213c7cf7" : {
      "Text_18" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Text_20" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Text_25" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Text_44" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Button_3" : [
        "f3c24340-051a-4949-b104-de091ffea568"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_8" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_9" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "9c0387ff-766a-487f-8264-526adbbf207e" : {
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "c6d4c233-dec6-4980-a388-e883e4bbe8b9" : {
      "Image_2" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_3" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_4" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_5" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_6" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_7" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_8" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_9" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Rectangle_1" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Rectangle_2" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Text_36" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Rectangle_3" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Label_29" : [
        "caa0fc67-2b43-42a7-88cc-bf2ca6f7fdc8"
      ],
      "Label_30" : [
        "9e0e72f4-eb07-47fc-8f92-56689676df9e"
      ],
      "Label_31" : [
        "8d6ab638-4bce-482f-b180-70d82e7df213"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ]
    },
    "1098cc94-5ef2-40f8-ae7b-a737dd890642" : {
      "Label_60" : [
        "bc9194cf-4d3e-49e0-829b-adaa213c7cf7"
      ],
      "Image_2" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_3" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_4" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_5" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_6" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_7" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_8" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_9" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Input_11" : [
        "eb7e4bb4-66b3-4fc2-b594-85686d8f546a"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Rectangle_12" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Button_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_8" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_9" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "dafda4bc-b018-4c3b-9683-c344feee5d07" : {
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
      "Catalog-Browse" : [
        "caa0fc67-2b43-42a7-88cc-bf2ca6f7fdc8"
      ],
      "Button_2" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Button_3" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Button_4" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ]
    },
    "387064e8-4dec-432a-85a3-91bdb9ec2705" : {
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "bddc04fb-0a00-4d73-8250-40b76a14e268" : {
      "Text_cell_2" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Text_cell_3" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Text_cell_16" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Text_cell_21" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Text_cell_26" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Text_cell_31" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Text_cell_36" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Image_2" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Image_4" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Image_6" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Image_8" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Image_10" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Image_12" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Image_14" : [
        "e5179c28-f634-4971-8db5-99a9cb58aa6d"
      ],
      "Button_1" : [
        "ce5b346a-2904-4724-a2b1-397bb374e184"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "b3e0a165-d2d6-4c8d-bff3-256e8fffc2e0" : {
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "4f8cd8be-62b9-496f-82b3-9cb295dd53bd" : {
      "Hotspot_2" : [
        "246d0410-edea-4da3-9af7-c856145710e2"
      ],
      "Hotspot_1" : [
        "246d0410-edea-4da3-9af7-c856145710e2"
      ]
    },
    "75963702-1f54-4fdf-a7aa-6684ddd180b2" : {
      "Hotspot_2" : [
        "4862ccc8-3824-4cbf-9fbb-89a3a496b0df"
      ],
      "Hotspot_1" : [
        "4862ccc8-3824-4cbf-9fbb-89a3a496b0df"
      ]
    },
    "b5d19c16-ab57-4e20-b8ee-b3e1851b524a" : {
      "Label_62" : [
        "e4f289e1-cd8d-44f4-b911-42fce351a3cf"
      ],
      "Text_8" : [
        "e4f289e1-cd8d-44f4-b911-42fce351a3cf"
      ]
    },
    "373ea280-a673-47c7-8f6b-3e51a5f2518a" : {
      "Hotspot_1" : [
        "f61420f4-0edf-48af-8504-e3daa782c825"
      ],
      "Hotspot_2" : [
        "f61420f4-0edf-48af-8504-e3daa782c825"
      ]
    },
    "5cd400c7-6a59-4cbc-b6e2-74a80189292c" : {
      "Button_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Button_4" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "865dc106-9535-4423-b66e-cc9c3570b79e" : {
      "Hotspot_1" : [
        "7c82acbb-8012-48b0-90f4-b02ace645e77"
      ],
      "Hotspot_2" : [
        "7c82acbb-8012-48b0-90f4-b02ace645e77"
      ]
    },
    "b434b8ed-70b1-4325-b4dd-447c6acc36b8" : {
      "Text_cell_2" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Text_cell_3" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Text_cell_16" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Text_cell_21" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Text_cell_26" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Text_cell_31" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Text_cell_36" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Button_1" : [
        "816d8f26-8520-4749-867f-c48008e0bbca"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ],
      "Label_2" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_7" : [
        "b434b8ed-70b1-4325-b4dd-447c6acc36b8"
      ],
      "Label_5" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ],
      "Image_19" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Image_22" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Image_24" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Image_26" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Image_28" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Image_30" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ],
      "Image_32" : [
        "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8"
      ]
    },
    "eb7e4bb4-66b3-4fc2-b594-85686d8f546a" : {
      "Rectangle_12" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_8" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_9" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "9e0e72f4-eb07-47fc-8f92-56689676df9e" : {
      "Label_29" : [
        "caa0fc67-2b43-42a7-88cc-bf2ca6f7fdc8"
      ],
      "Label_31" : [
        "8d6ab638-4bce-482f-b180-70d82e7df213"
      ],
      "Hotspot_1" : [
        "c6d4c233-dec6-4980-a388-e883e4bbe8b9"
      ],
      "Hotspot_2" : [
        "c6d4c233-dec6-4980-a388-e883e4bbe8b9"
      ],
      "Hotspot_3" : [
        "c6d4c233-dec6-4980-a388-e883e4bbe8b9"
      ],
      "Hotspot_4" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ]
    },
    "e2befb41-ce34-467e-b1f0-76742ef625c5" : {
      "Label_61" : [
        "b88b8333-d88c-45d1-a95c-f59835b3a81e"
      ],
      "Text_cell_52" : [
        "b88b8333-d88c-45d1-a95c-f59835b3a81e"
      ],
      "Text_cell_44" : [
        "b88b8333-d88c-45d1-a95c-f59835b3a81e"
      ],
      "Text_cell_48" : [
        "b88b8333-d88c-45d1-a95c-f59835b3a81e"
      ],
      "Text_cell_54" : [
        "b88b8333-d88c-45d1-a95c-f59835b3a81e"
      ],
      "Image_19" : [
        "b88b8333-d88c-45d1-a95c-f59835b3a81e"
      ],
      "Image_22" : [
        "b88b8333-d88c-45d1-a95c-f59835b3a81e"
      ],
      "Image_24" : [
        "b88b8333-d88c-45d1-a95c-f59835b3a81e"
      ],
      "Image_26" : [
        "b88b8333-d88c-45d1-a95c-f59835b3a81e"
      ]
    },
    "e4f289e1-cd8d-44f4-b911-42fce351a3cf" : {
      "Text_cell_52" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Text_cell_44" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Text_cell_48" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Text_cell_54" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Text_cell_58" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Text_cell_64" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Text_cell_68" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Text_cell_74" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Text_cell_78" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Text_5" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ],
      "Image_19" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Image_22" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Image_24" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Image_26" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Image_28" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Image_38" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Image_40" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Image_42" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ],
      "Image_44" : [
        "b5d19c16-ab57-4e20-b8ee-b3e1851b524a"
      ]
    },
    "4e565045-83b8-489e-9f4c-5c2c86d8ece5" : {
      "Text_cell_2" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Text_cell_3" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Text_cell_16" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Text_cell_21" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Text_cell_26" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Text_cell_31" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Text_cell_36" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Image_2" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Image_4" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Image_6" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Image_8" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Image_10" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Image_12" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Image_14" : [
        "152edac9-417e-44ad-a719-06621542766b"
      ],
      "Button_1" : [
        "5cd400c7-6a59-4cbc-b6e2-74a80189292c"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "46d064de-69cb-4bdb-9d49-baaf4b83baa9" : {
      "Text_cell_2" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Text_cell_3" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Text_cell_16" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Text_cell_21" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Text_cell_26" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Text_cell_31" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Text_cell_36" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Image_2" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Image_4" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Image_6" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Image_8" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Image_10" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Image_12" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Image_14" : [
        "9c0387ff-766a-487f-8264-526adbbf207e"
      ],
      "Button_1" : [
        "9fd81e90-b7a6-430c-9943-1f735e84b0a4"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "b88b8333-d88c-45d1-a95c-f59835b3a81e" : {
      "Label_62" : [
        "e2befb41-ce34-467e-b1f0-76742ef625c5"
      ],
      "Text_10" : [
        "e2befb41-ce34-467e-b1f0-76742ef625c5"
      ]
    },
    "e5179c28-f634-4971-8db5-99a9cb58aa6d" : {
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ],
      "Button_7" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ]
    },
    "b9886d29-cd18-4b43-ac17-49705729344f" : {
      "Button_1" : [
        "e017b744-8cf9-47b5-a3e0-f153321117e7"
      ],
      "Hotspot_2" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_3" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_4" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_5" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_6" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_7" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_8" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_9" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_10" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_11" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_12" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_13" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_14" : [
        "6d8bb5b6-b0be-4f58-8b52-6493cd4e896d"
      ],
      "Hotspot_15" : [
        "3208cb01-2887-4b59-98d3-6b631eba0a60"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "dc4a4ef3-1474-4b76-8c51-754fca7c9caa" : {
      "Image_2" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_3" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_4" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_5" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_6" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_7" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_8" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Image_9" : [
        "9240d9b5-29b6-457e-9d2c-b8fb0f4e3fac"
      ],
      "Text_45" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ],
      "Input_11" : [
        "eb7e4bb4-66b3-4fc2-b594-85686d8f546a"
      ]
    },
    "13360058-8212-423c-a338-d904df2074a4" : {
      "Hotspot_1" : [
        "ef016c51-f4bb-4ea9-b0c9-81f8e2a0c574"
      ],
      "Hotspot_2" : [
        "ef016c51-f4bb-4ea9-b0c9-81f8e2a0c574"
      ]
    },
    "f0681338-9481-448b-889a-96e76a81eab8" : {
      "Hotspot_6" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_1" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_2" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_3" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_4" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_5" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_8" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_9" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_10" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_11" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_12" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Hotspot_14" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a" : {
      "Text_cell_2" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Text_cell_3" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Text_cell_16" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Text_cell_21" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Text_cell_26" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Text_cell_31" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Text_cell_36" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Image_2" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Image_4" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Image_6" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Image_8" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Image_10" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Image_12" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Image_14" : [
        "387064e8-4dec-432a-85a3-91bdb9ec2705"
      ],
      "Button_1" : [
        "55302c98-dacd-409b-aca9-b66bde66bb22"
      ],
      "Text_43" : [
        "62052b62-0e08-4385-b811-459d6e3bd458"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "dc4a4ef3-1474-4b76-8c51-754fca7c9caa"
      ],
      "Label_2" : [
        "bddc04fb-0a00-4d73-8250-40b76a14e268"
      ],
      "Label_3" : [
        "4e565045-83b8-489e-9f4c-5c2c86d8ece5"
      ],
      "Label_1" : [
        "8a9c867b-1bad-4d75-9d7d-ab845d5dd386"
      ],
      "Label_5" : [
        "bfd8a09d-f6a3-4007-8297-cd8bf061ef3a"
      ],
      "Label_4" : [
        "b7660e1d-2981-480e-a434-643a6005623d"
      ]
    },
    "338a1b2a-de53-422c-9903-6796360c9db6" : {
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ],
      "Label_2" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_1" : [
        "b434b8ed-70b1-4325-b4dd-447c6acc36b8"
      ],
      "Label_5" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Text_49" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ]
    },
    "ef016c51-f4bb-4ea9-b0c9-81f8e2a0c574" : {
      "Hotspot_6" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_1" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_2" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_3" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_4" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_5" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_8" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_9" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_10" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_11" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_12" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Hotspot_13" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "47f199bb-ed47-48c3-9b3c-91d46b2f5f55" : {
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "dcc5a46f-47b9-46c6-8eb0-b772ddb1347c" : {
      "Hotspot_6" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_1" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_2" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_3" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_4" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_5" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_8" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_9" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_10" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_11" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_12" : [
        "d5b0709b-9d8b-4dfb-8daa-ed31d05d90d8"
      ],
      "Hotspot_13" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "f61420f4-0edf-48af-8504-e3daa782c825" : {
      "Hotspot_6" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_1" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_2" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_3" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_4" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_5" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_8" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_9" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_10" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_11" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_12" : [
        "373ea280-a673-47c7-8f6b-3e51a5f2518a"
      ],
      "Hotspot_13" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "86d0664d-6275-4d0a-b14e-6f431b938065" : {
      "Image_2" : [
        "f46e63cb-1ef1-4ec1-b53f-5535c09ec6eb"
      ],
      "Image_4" : [
        "f46e63cb-1ef1-4ec1-b53f-5535c09ec6eb"
      ],
      "Image_6" : [
        "f46e63cb-1ef1-4ec1-b53f-5535c09ec6eb"
      ],
      "Image_8" : [
        "f46e63cb-1ef1-4ec1-b53f-5535c09ec6eb"
      ],
      "Image_10" : [
        "f46e63cb-1ef1-4ec1-b53f-5535c09ec6eb"
      ],
      "Image_12" : [
        "f46e63cb-1ef1-4ec1-b53f-5535c09ec6eb"
      ],
      "Image_14" : [
        "f46e63cb-1ef1-4ec1-b53f-5535c09ec6eb"
      ],
      "Button_1" : [
        "0bc83ef7-b6c9-4707-842b-ec178414f6d4"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_8" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_7" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ]
    },
    "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac" : {
      "Hotspot_3" : [
        "f0681338-9481-448b-889a-96e76a81eab8"
      ],
      "Hotspot_4" : [
        "f0681338-9481-448b-889a-96e76a81eab8"
      ]
    },
    "c16b2b96-a4eb-429e-9f14-c05c7bd9bdd8" : {
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ],
      "Label_2" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_7" : [
        "b434b8ed-70b1-4325-b4dd-447c6acc36b8"
      ],
      "Label_5" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ]
    },
    "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928" : {
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "46eb1013-e2d1-477b-a6e6-c797d3c4a4bb" : {
      "Button_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Button_4" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "73c7c0cf-7f02-4824-8e07-2e47e872d956" : {
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_8" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_9" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ],
      "Rectangle_1" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Image_19" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Text_23" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Image_20" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Image_21" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Button_1" : [
        "47f199bb-ed47-48c3-9b3c-91d46b2f5f55"
      ]
    },
    "e017b744-8cf9-47b5-a3e0-f153321117e7" : {
      "Button_3" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Button_4" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "9fd81e90-b7a6-430c-9943-1f735e84b0a4" : {
      "Button_2" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Button_3" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "ab7dbcdc-517f-424b-a5f8-e6ec8541b06a" : {
      "Hotspot_1" : [
        "f61420f4-0edf-48af-8504-e3daa782c825"
      ],
      "Hotspot_2" : [
        "f61420f4-0edf-48af-8504-e3daa782c825"
      ]
    },
    "f3c24340-051a-4949-b104-de091ffea568" : {
      "Label_29" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Text_18" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Text_20" : [
        "e88b67fd-4eeb-465e-8a57-1e7d2ce31dac"
      ],
      "Text_25" : [
        "13360058-8212-423c-a338-d904df2074a4"
      ],
      "Text_44" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Text_62" : [
        "bc9194cf-4d3e-49e0-829b-adaa213c7cf7"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_8" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_9" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ],
      "Button_5" : [
        "bc9194cf-4d3e-49e0-829b-adaa213c7cf7"
      ],
      "Button_4" : [
        "bc9194cf-4d3e-49e0-829b-adaa213c7cf7"
      ]
    },
    "b5d3ee73-97b8-42cc-8648-46a63c7ef8b1" : {
      "Button_3" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Button_4" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "f46e63cb-1ef1-4ec1-b53f-5535c09ec6eb" : {
      "Button_7" : [
        "dafda4bc-b018-4c3b-9683-c344feee5d07"
      ],
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_8" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_7" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Text_43" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ]
    },
    "246d0410-edea-4da3-9af7-c856145710e2" : {
      "Hotspot_7" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Label_60" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Hotspot_1" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ],
      "Hotspot_2" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Hotspot_3" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Hotspot_4" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Hotspot_5" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Hotspot_6" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Hotspot_9" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Hotspot_10" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Hotspot_11" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Hotspot_12" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ],
      "Hotspot_13" : [
        "4f8cd8be-62b9-496f-82b3-9cb295dd53bd"
      ]
    },
    "84ccef2a-002c-4702-abdc-665bf9307246" : {
      "Label_6" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_7" : [
        "84ccef2a-002c-4702-abdc-665bf9307246"
      ],
      "Label_2" : [
        "86d0664d-6275-4d0a-b14e-6f431b938065"
      ],
      "Label_1" : [
        "b434b8ed-70b1-4325-b4dd-447c6acc36b8"
      ],
      "Label_5" : [
        "3e3fd30c-21e7-49d8-9334-f6f956edb8ee"
      ],
      "Image_2" : [
        "338a1b2a-de53-422c-9903-6796360c9db6"
      ],
      "Image_3" : [
        "338a1b2a-de53-422c-9903-6796360c9db6"
      ],
      "Image_4" : [
        "338a1b2a-de53-422c-9903-6796360c9db6"
      ],
      "Image_5" : [
        "338a1b2a-de53-422c-9903-6796360c9db6"
      ],
      "Image_6" : [
        "338a1b2a-de53-422c-9903-6796360c9db6"
      ],
      "Image_7" : [
        "338a1b2a-de53-422c-9903-6796360c9db6"
      ],
      "Image_8" : [
        "338a1b2a-de53-422c-9903-6796360c9db6"
      ],
      "Image_9" : [
        "338a1b2a-de53-422c-9903-6796360c9db6"
      ],
      "Text_45" : [
        "63fdf96d-7f58-42cd-997e-788220726745"
      ]
    },
    "0443c108-a6d5-4f3e-bfbf-a1d249d14d09" : {
      "Label_8" : [
        "d8b4ac47-3750-4dd2-9882-69163287b8ff"
      ],
      "Label_7" : [
        "e6c7eed4-c07c-4af4-a794-a03fe16757a2"
      ],
      "Label_6" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ],
      "Label_10" : [
        "9ea92e9f-f86c-4adc-a9b8-31fcfab9b928"
      ],
      "Label_11" : [
        "0443c108-a6d5-4f3e-bfbf-a1d249d14d09"
      ],
      "Label_3" : [
        "2fd50b8b-8591-4968-a722-5f7a2a79dfb4"
      ],
      "Label_1" : [
        "98f246bc-68da-4efe-95cc-f69e6b0545b9"
      ],
      "Label_5" : [
        "b9886d29-cd18-4b43-ac17-49705729344f"
      ],
      "Label_4" : [
        "46d064de-69cb-4bdb-9d49-baaf4b83baa9"
      ],
      "Label_2" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    },
    "4439af3b-d7e8-4468-8725-eb1205194216" : {
      "Label_29" : [
        "caa0fc67-2b43-42a7-88cc-bf2ca6f7fdc8"
      ],
      "Hotspot_1" : [
        "1098cc94-5ef2-40f8-ae7b-a737dd890642"
      ]
    },
    "4862ccc8-3824-4cbf-9fbb-89a3a496b0df" : {
      "Hotspot_6" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_1" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_2" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_3" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_4" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_5" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_8" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_9" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_10" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_11" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_12" : [
        "75963702-1f54-4fdf-a7aa-6684ddd180b2"
      ],
      "Hotspot_13" : [
        "ee7c5d6c-77ca-4655-a23b-6f8ad2bf2b96"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);