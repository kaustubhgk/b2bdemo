(function(window, undefined) {

    /*********************** START STATIC ACCESS METHODS ************************/

    jQuery.extend(jimMobile, {
        "loadScrollBars": function() {
            jQuery(".s-0f92d7ee-bc84-4a0e-a625-378331c65f7b .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-44d21804-245b-4a6d-86d1-1ca8740f98b3 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-4fdee2fb-1b2b-43bd-9ad9-401ca33d883b .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-4a203ae6-1ad5-4d2a-8a7b-7fa46fa373a3 .ui-page").overscroll({ showThumbs:true, direction:'multi' });
            jQuery(".s-a5eab988-83fd-4bb2-904b-1ad6edfa3cc5 .ui-page").overscroll({ showThumbs:true, direction:'multi' });
            jQuery(".s-c8762469-a365-4377-9ee5-adb935135af3 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-0b71751b-f89b-4cf3-be7f-38ede40b914e .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-2fde4256-c9a7-408c-90ca-9ef2a2e9c3b4 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-e5bae985-dc99-49b2-bdfa-39c1746c6545 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-891e9e71-852e-40f9-9b8d-f028ade55785 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-891e9e71-852e-40f9-9b8d-f028ade55785 #s-Category_1").overscroll({ showThumbs:false, direction:'vertical' });
            jQuery(".s-f1761c56-cbe8-4452-b0e1-63b918b4359a .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-4652f025-69dc-432b-baa4-c251192d84c5 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-48a90c96-d590-428d-afd0-f7771333419b .ui-page").overscroll({ showThumbs:true, direction:'multi' });
            jQuery(".s-635517ea-5035-4458-b8fa-961907424317 .ui-page").overscroll({ showThumbs:true, direction:'multi' });
            jQuery(".s-f1154b08-c10c-4fb9-a217-3c480fe24cb6 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-d12245cc-1680-458d-89dd-4f0d7fb22724 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-5a6af0ff-0dea-449f-9fa4-c022009ec615 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-b33f31ff-8c70-4ebd-8f71-f74a47803b88 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-b3080130-feea-4c36-98b8-e1c0658b381e .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-788f4e71-7027-40ac-8f2f-e61c313d3adc .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-28126891-fa23-4e0b-b0f8-158d49d696a7 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-f80720ed-6e33-40f6-ada6-1d0048a121b0 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-514a84fe-c9f5-4173-8a97-c748dbd79717 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-16892c0b-c777-421f-96bd-ca6f5e8bdcb8 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-78e679e7-63b3-42b3-a08d-031ec0dbb7bb .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-95a314d0-999c-4f27-8691-d06306e2a889 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-7093ee06-cf23-4348-8dd6-347d9dc1215c .ui-page").overscroll({ showThumbs:true, direction:'multi' });
            jQuery(".s-f39d2b82-4c13-4e93-9c3a-b328191e3c82 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-ef9398aa-8cf9-4a6b-9743-3b96e5e7cb23 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-7885cb0e-f270-4fd4-8fa6-d84ae8e4c864 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-bfb80ed2-d9ae-41a0-91fd-73fc1cd3bb06 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-2c3bef71-07f3-439a-918d-390553e599b3 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-de613892-a57b-40ec-91bd-f96c7644a8fc .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-24b9e92b-5471-41a8-93ba-a93c791c1486 .ui-page").overscroll({ showThumbs:true, direction:'multi' });
            jQuery(".s-1e6eafb7-f94a-46d4-88b1-5303ecc2382f .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-1e6eafb7-f94a-46d4-88b1-5303ecc2382f #s-Category_1").overscroll({ showThumbs:false, direction:'vertical' });
            jQuery(".s-45d7eede-955e-4165-aef2-d1e9288345b1 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-aa97fc2e-898d-4726-ab91-a05c0be38ea7 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-aa97fc2e-898d-4726-ab91-a05c0be38ea7 #s-Category_1").overscroll({ showThumbs:false, direction:'vertical' });
            jQuery(".s-724487a0-fba2-4922-9d2d-c799726d76a8 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-b16d57ba-5511-42d3-9514-12887ec396ee .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-45b04505-07f4-450b-bc77-9adadbaae106 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-3f32e719-15a0-40de-a623-f9b9495ac989 .ui-page").overscroll({ showThumbs:true, direction:'multi' });
            jQuery(".s-a559a502-3e90-43d6-b2da-3f531938cf07 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-1a16dc39-46af-4ecc-ab3f-98983f472e42 .ui-page").overscroll({ showThumbs:true, direction:'multi' });
            jQuery(".s-c63cd663-1041-4aab-8105-4a4bb5e1c2d3 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-c63cd663-1041-4aab-8105-4a4bb5e1c2d3 #s-Category_1").overscroll({ showThumbs:false, direction:'vertical' });
            jQuery(".s-367f215b-efe1-43a9-a2c2-b162bc92d842 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-c39fe0c4-a087-4115-9ad3-5868d483fdd0 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-e9c26eb4-6187-4d14-bd14-93ec32cbcbce .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-b4fded73-af93-4517-a07c-aaba3efdf270 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-84bed901-e8ab-44a7-9b11-114627bc5317 .ui-page").overscroll({ showThumbs:true, direction:'multi' });
            jQuery(".s-c008f2fb-7e7c-49f5-8a84-2cef73e08207 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-fd434ea4-7958-4295-ba69-aa1335b7af6e .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-fd434ea4-7958-4295-ba69-aa1335b7af6e #s-Category_1").overscroll({ showThumbs:false, direction:'vertical' });
            jQuery(".s-d0695d5b-2753-4d21-ae6c-999d25388591 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
            jQuery(".s-bcc90db0-da1a-44f1-9990-a8835bcce188 .ui-page").overscroll({ showThumbs:true, direction:'vertical' });
         }
    });

    /*********************** END STATIC ACCESS METHODS ************************/

}) (window);